const express = require('express')
const router = express.Router()
const Profiles = require('../../modules/profile')
const passport = require('passport')

/**
 * @route  /profils/test
 * @describe  测试
 * @ access  public
 */
router.get('/test', (req, res) => {
    res.json({ msg: 'hello profiles test' })
})


/**
 * @route /profiles/add
 * @describe 添加个人数据
 * @ access prevate
 */
//passport.authenticate('jwt', { session: false }) 因为是私有接口 
// 所以请求的时候需要进行token验证 这条就是做token验证的,会跳到passport.js模块中去进行验证
router.post('/add', passport.authenticate('jwt', { session: false }), (req, res) => {
    const profileInfo = {}
    const obj = req.body
    // 枚举 请求体对象 把有值的属性添加到profileInfo对象中,用这个对象添加到数据库中作为document
    for (i in obj) {
        // console.log(i)
        // console.log(obj[i])
        if (obj[i]) {
            profileInfo[i] = obj[i]
        }

    }
    console.log(profileInfo)
    //使用Profiles cellction对象创建document对象
    Profiles.create(profileInfo)

    res.json({code:1,msg:'添加成功'})
})


/**
 * @route /profiles  
 * @describe 查询获取个人所有数据信息
 * @ access prevate
 */
router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profiles.find()
        .then(profiles => {
            if (!profiles) {
                res.status(404).json('未能查询到用户数据')
            } else {
                res.json(profiles)
            }
        })
        .catch(err => res.status(404).json(err))
})


/**
 * @route /profiles/:id
 * @describe 查询单条数据信息
 * @ access prevate
 * 
 */
///:id 占位符 由前端传递过来  用这个去数据库里查 在req.params中可以拿到
router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const id = req.params.id
    // 用 id去查询数据  
    Profiles.findOne({ _id: id })
        .then(data => {
            if (!data) {
                res.status(404).json('未能查询到数据')
            } else {
                res.json(data)
            }
        })
        .catch(err => res.status(404).json('未能查询到数据'))

})


/**
 * @route /profiles/edit/:id
 * @describe 更新用户某条数据信息
 * @ access prevate
 */
router.post('/edit/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    //查询当前数据更新为新的数据
    // Profiles.findOne({ _id: req.params.id})
    // .then(data => {
    //     if(data){
    //         res.status(404).json('未能查询到数据')
    //     }else{
    //         Profiles.updateOne()
    //     }

    // })      
    // 值: 1查询条件 2 更新的内容 3 true返回更新后的值 false返回更新前的值
    Profiles.updateOne({ _id: req.params.id }, req.body)
        .then(data => {
            if (!data) {
                res.status(404).json('更新数据失败')
            } else {
                res.json({code:1,msg:'更新数据成功'})
            }
        })
        //res.status(404).json(err)
        .catch(err => res.status(404).json(err))
})


/**
 * @route /profiles/delete/:id
 * @describe 删除当前数据
 * @ access prevate
 */

router.delete(
    '/delete/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        Profiles.findOneAndDelete({ _id: req.params.id })
            .then(response => {
                // 删除方法 删除成功会返回那条被删除的数据 这里的response就是
                if (!response) {
                    res.status(404).json('删除失败')
                } else {
                    res.json({code:1,msg:'删除成功'})
                }
            })
            .catch(err => res.status(404).json(err))
    }
)

module.exports = router