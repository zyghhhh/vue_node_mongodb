// 用户访问接口得模块  

const express = require('express')
const router = express.Router();
const User = require('../../modules/user')
const md5 = require('md5')
const gravatar = require('gravatar');
const jwt = require('jsonwebtoken')
const passport = require('passport')

router.get('/test', (req, res) => {
    res.json({ msg: 'yes' })
})

/**
 *  
 * 
 */
router.post('/register', (req, res) => {
    console.log('请求体=' + req.body)
    //全球公认头像api       
    // d的属性为mm 的话 会返回一个连接 这个是全球公认头像给返回的一个连链接 如果是已经注册过的邮箱就会有一个上传过的头像
    const avatar = gravatar.url(req.body.email, { s: '200', r: 'pg', d: 'mm' });
    //查询User
    User.findOne({ email: req.body.email })
        .then((user) => {
            //查询到了 
            // 有两种情况 1邮箱已被注册  2邮箱没有被注册 所以这里用if判断
            if (user) {
                // 代表已经注册过  
                return res.status(400).json('邮箱已被注册')

            } else {
                // 代表没有注册过   这里添加数据到数据库
                User.create({
                    name: req.body.name,
                    email: req.body.email,
                    avatar,
                    password: req.body.password,
                    identity: req.body.identity
                }).then(() => {
                    res.json({
                        code: 1,
                        msg: '注册成功'
                    })
                })


            }
        })

})

/**
 *  $route POST  /users/login
 *  desc    返回token jwt(json web token)
 *  access  public 
 */
router.post('/login', (req, res) => {
    // 逻辑: 登陆时使用邮箱和密码进行登录
    //    就需要拿到邮箱和密码 先查询邮箱是否注册过 注册过的话再匹配密码,正确了就返回token 
    const email = req.body.email
    const password = req.body.password
    // 去数据库中查询
    User.findOne({ email })
        .then(user => {
            // user是查询返回的结果  如果查询到了就返回一条数据  查询不到就是Null
            if (!user) {
                //没有查到就返回404  和一条错误信息
                return res.status(404).json({ code: 0, msg: '账号或密码不正确,请确认后重试' })
            } else {
                //查询到了数据 证明注册过 就去匹配密码是否正确
                if (user.password === password) {
                    //------------密码也正确 生成token返回 用jwt库的sign方法
                    //token规则
                    const rule = {
                        id: user.id,
                        name: user.name,
                        avatar: user.avatar,
                        identity: user.identity
                    }
                    //生成token方法 四个值 1 规则 2 name 3 过期时间  4箭头函数
                    jwt.sign(rule, 'secret', { expiresIn: 3600 }, (err, token) => {
                        if (err) throw err
                        res.json({
                            code: 1,
                            data: {
                                success: true,
                                //验证token的时候必须要用 Bearer (后面有个空格)  + token这样的token格式去验证,否则出错
                                token: 'Bearer ' + token,
                            }


                        })
                    })
                } else {
                    return res.status(404).json({ code: 0, msg: '账号或密码不正确,请确认后重试' })
                }

            }

        })
})

/**
 *  $route get  /users/current
 *  desc    return token
 *  access  prevate
 */
// 想要拿到数据库中用户的数据 就要验证token 
//                     passport.authenticate('jwt',{session:false}) 验证方法 参数1 jwt(就是token) , 2 {session:false}
//               当访问到current 后 先去../config/passport.js中用token去验证,验证通过后会返回用户信息,在回调中返回
router.get('/current', passport.authenticate('jwt', { session: false }), (req, res) => {
    //给用户返回id name email        
    res.json({
        //user 是 passport验证通过后 返回的user 也就是整条用户信息
        id: req.user.id,
        name: req.user.name,
        email: req.user.email,
        identity: req.user.identity,
    })
})


module.exports = router