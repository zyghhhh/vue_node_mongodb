//个人数据信息规则模块

const mongoose = require('mongoose')

const Profile = mongoose.Schema({
    type:{
        type:String
    },
    describe:{
        type:String
    },
    income:{
        type:String,
        required:true    
    },
    expend:{
        type:String,
        required:true
    },
    cash:{
        type:String,
        required:true
    },
    remark:{
        type:String

    },
    date:{
        type:Date,
        default:Date.now
    }
})

module.exports = Profiles = mongoose.model('Profiles', Profile)
