// 
const mongoose = require('mongoose')

//创建集合规则
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    avatar:{
        type:String,
        required:false
    },
    password:{
        type:String,
        required:true
    },
    identity: {
        type: String,
        required: true
    },
    date:{
        type:Date,
        default:Date.now,
    },
})

//用规则去创建文档
module.exports = User = mongoose.model('Users',userSchema)

