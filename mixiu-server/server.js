const app = require('express')()
const mongoose  = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')

// 引入接口路由模块
const users = require('./router/api/users')
const profiles = require('./router/api/profiles')

//初始化一个端口号
const port = process.env.PORT || 5000;
const db = require('./config/keys').mongoURI


// mongodb+srv://zyg:zhang..0@cluster0-ngvk1.mongodb.net/test?retryWrites=true&w=majority
// mongoose.connect('mongodb+srv://zyg:zhang..0@cluster0-ngvk1.mongodb.net/test?retryWrites=true&w=majority',{useUnifiedTopology:true,useNewUrlParser:true})
mongoose.connect(db,{useUnifiedTopology:true,useNewUrlParser:true})
    .then(() => console.log('数据库连接成功'))
    .catch(err => console.log(err)) 

require('./config/passport')(passport)



// 使用body-parser插件处理post请求请求体中的数据为表单格式
app.use(bodyParser.urlencoded({urlencoded:false}))     // char-www.form.urlencoded 变成json对象 放到req.body上
app.use(bodyParser.json())  // 把客户端传递过来的json字符串 变成 json对象 放到req.body上
app.use(passport.initialize())  //passport初始化 不然没法使用


// 匹配路径,处理接口请求
//  使用route 定义接口        访问/api/users 就会访问到 users这个模块
app.use('/users',users)
app.use('/profiles', profiles)


//监听端口
app.listen(port,() => {
    console.log(`Server running on port ${port}`)
})
