//格林尼治2019-03-19T16:00:00.000Z  ==>>  2019-03-20 00:00:00   与北京时间8小时时差
//格林尼治2019-03-19T16:00:00.000Z  ==>>  2019-03-20 00:00:00 
// //时间戳1553547600000  转 //  2019-03-25T21:00:00.000Z
export const formDate = (dateForm) => {
    if (dateForm === "") {  //解决deteForm为空传1970-01-01 00:00:00
        return "";
    } else {
        var dateee = new Date(dateForm).toJSON();
        var date = new Date(+new Date(dateee) + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '');
        return date;
    }
}
// console.log(formDate("2019-03-19T16:00:00.000Z"))//2019-03-20 00:00:00
// console.log(formDate(1553547600000))             //2019-03-26 05:00:00
// console.log(formatDateT(1553547600000))          //2019-03-25T21:00:00.000Z

//时间戳1553547600000  转 //  2019-03-25T21:00:00.000Z
export const formatDateT = (dataTime) => {
    // var timestamp3 = 1551686227000; 
    var timestamp = dataTime;
    var newDate = new Date(dataTime);
    // var newDate = new Date(dataTime + 8 * 3600 * 1000 );               

    newDate.getTime(timestamp * 1000);
    // console.log(newDate.toDateString());//Mon Mar 11 2019                
    // console.log(newDate.toGMTString()); //Mon, 11 Mar 2019 06:55:07 GMT                
    // console.log(newDate.toISOString()); //2019-03-11T06:55:07.622Z
    return newDate.toISOString()
}