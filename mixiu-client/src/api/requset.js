import qs from 'qs'
import axios from 'axios'
import { Message,Loading } from 'element-ui';
import router from './../router'

var ip = '/api'
const service = axios.create({
    baseURL: ip,  //请求地址
    timeout: 5000, //超时时间
})

let loadingInstance

service.interceptors.request.use(config => {
    // 给需要带请求头的请求携带请求头
    if (localStorage.getItem('token')){
        config.headers.authorization = localStorage.getItem('token')
    }

    // console.log(config)
    //我的做法比较简单值判断了请求方式做了相应处理
    //有更多需求可以做其他配置 详情百度
    config.method === 'post'
        ? config.data = qs.stringify({ ...config.params })
        : config.data = { ...config.params }


    // console.log(config)

        //开始加载动画
    loadingInstance = Loading.service({
        text:'拼命加载中...',
        background:'rgba(255,255,255,.7)'
    });
    
    // console.log(config)

    return config    //返回结果
}, error => {
    Promise.rejecet(error)
})


service.interceptors.response.use(
    response => {
        // console.log(response)
        //处理后端数据
        if (response.status === 200) {

            //关闭加载动画
            loadingInstance.close()

            //在这里面可以做很多处理比如http状态码的判断,由于我比较懒就没弄
            return response.data
        }
    },
    error => {
        //关闭加载动画
        loadingInstance.close()
        //获取请求状态码
        const { status } = error.response
        if (status == 401){
            console.log('401')
            Message.error('身份过期,请重新登陆')
            //401代表token过期
            localStorage.removeItem('token')
            router.replace('/login')
        }
        //做失败处理
        return Promise.reject(error)
    }
)


export default service