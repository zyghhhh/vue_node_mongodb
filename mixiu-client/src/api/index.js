import service from './requset'

export const reqRegister = params => {
    // console.log(params)
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: '/users/register',
        //请求方式
        method: 'post',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        params
    })
}  



export const reqLogin = params => {
    // console.log(params)
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: '/users/login',
        //请求方式
        method: 'post',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        params
    })
}  


export const reqProfiles = () => {
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: '/profiles',
        //请求方式
        method: 'get',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        // params
    })
}  


export const reqAddProfile = params => {
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: '/profiles/add',
        //请求方式
        method: 'post',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        params
    })
} 


export const reqEditProfile = params => {
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: `/profiles/edit/${params._id}`,
        //请求方式
        method: 'post',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        params
    })
} 

export const reqRemoveProfile = params => {
    // console.log(JSON.parse(params))  //打印 data : {type:'1'}
    return service({
        //请求地址  这里只有路由后缀,因为到拦截器中地址会进行拼串,详情可以在拦截器模块中打印config查看
        url: `/profiles/delete/${params._id}`,
        //请求方式
        method: 'delete',
        //请求参数 这里说明 get请求接受的就是params post就是data 踩过坑了
        params
    })
} 