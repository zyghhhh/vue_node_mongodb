import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// import { Message}from 'element-ui'
// import { Button, Select, Form, FormItem, Input,Option,Message,Row,Col,Dropdown,DropdownItem,DropdownMenu} from 'element-ui'
// Vue.component(Button.name, Button);
// Vue.component(Select.name, Select);
// Vue.component(Form.name, Form);
// Vue.component(FormItem.name, FormItem);
// Vue.component(Input.name, Input);
// Vue.component(Option.name, Option);
// Vue.component(Row.name, Row);
// Vue.component(Col.name, Col);
// Vue.component(Dropdown.name, Dropdown);
// Vue.component(DropdownItem.name, DropdownItem);
// Vue.component(DropdownMenu.name, DropdownMenu);


Vue.config.productionTip = false
// Vue.prototype.$message = Message;
Vue.use(ElementUI);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
