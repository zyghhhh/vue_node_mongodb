import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)



const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('@/views/Index/Index'),
    children: [
      { path: '/infoShow', name: 'infoShow', component: () => import('@/views/InfoShow/InfoShow') },
      { path: '', name: 'home', component: () => import('@/views/Home/Home') },
      { path: '/home', name: 'home', component: () => import('@/views/Home/Home') },
      { path: '/fundList', name: 'fundList', component: () => import('@/views/FundList/FundList') }
    ]

  },
  // { path: '/index/home', name: 'home', component: () => import('@/views/Home/Home') }
,
  {
    path: '/register',
    name: 'register',
    component: () => import('@/views/Register/Register')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login/Login')
  },
  {
    path: '*',
    name: '404',
    component: () => import('@/views/404')
  },
]




const router = new VueRouter({
  routes
})


router.beforeEach((to, from, next) => {
  // to 要到的路由  from过来的路由  next() 调用才会执行
  //做权限判断 是否可以进入主页面
  if (to.path === '/login' || to.path === '/register') {
    next()
  } else {
    const token = localStorage.getItem('token') ? true : false
    if (token) {
      next()
    } else {
      next('/login')
    }
  }
})

export default router
