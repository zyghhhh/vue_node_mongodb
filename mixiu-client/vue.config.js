
module.exports = {
   
    lintOnSave: false, // 是否开启eslint保存检测，有效值：ture | false | 'error'
    
    devServer: {
        open: true,
        host: 'localhost',
        port: 8081,
        https: false,
        hotOnly: false,
        proxy: { // 配置跨域
            '/api': {
                target: 'http://localhost:5000',
                ws: true,
                changOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        },
        
    }
}
